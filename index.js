/*
1. In the S27 folder, create an a1 folder and an index.js file inside of it.
2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
3. Test all the endpoints in Postman.
4. Create a git repository named S27.
5. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
6. Add the link in Boodle.
*/

const http = require("http");

const port = 4000;

http.createServer((request, response) => {
	
	// b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!
	if(request.url == '/profile'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to your profile");
	}
	
	// c. If the url is http://localhost:4000/courses, send a response Here’s our courses available 
	else if(request.url == '/courses'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Here's our courses available");
	}
	// d. If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
	else if(request.url == '/addCourse'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Add course to our resources");
	}
	// e. If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
	else if(request.url == '/updateCourse'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Update course to our resources");
	}
	// f. If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
	else if(request.url == '/archiveCourse'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Archive course to our resources");
	}
	
	else{
	// a. If the url is http://localhost:4000/, send a response Welcome to Booking System
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("Welcome to our Booking System");
	}
}).listen(port);

console.log(`Server now accessible at localhost: ${port}`);



